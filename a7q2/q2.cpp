#include <stdio.h>
#include <ctype.h>
#define STOP '|'
int main(void)
{
	char c, last;
	int numword = 0, numupper = 0, numlower = 0, numpunc = 0, numdigit = 0;
	printf("Enter text to be counted(| to stop).:\n");
	while ((c  = getchar())!= STOP)
	{
		if (isupper(c))
		{
			numupper++;
		}

		if (islower(c))
		{
			numlower++;
		}

		if ((!isalpha(c) && !isdigit(c) && !isspace(last) && !ispunct(c)) || c ==STOP)
		{
			numword++;
		}

		if (isdigit(c))
		{
			numdigit++;
		}

		if (ispunct(c))
		{
			numpunc++;
		}
		last = c;
	}
	printf("There are %i words, %i uppercase letters, %i lowercase letters, %i punctuation characters and %i digits", numword, numupper, numlower, numpunc, numdigit);
	return 0;
}