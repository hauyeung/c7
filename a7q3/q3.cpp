#include <stdio.h>
#include <ctype.h>
#define STOP '|'
int main(void)
{
	char c, last;
	char filename[256];
	FILE *file;
	printf("Enter enter your file name:\n");
	scanf("%s", &filename);
	file = fopen(filename, "r");
	int numword = 0, numupper = 0, numlower = 0, numpunc = 0, numdigit = 0;
	if (file)
	{
		while ((c  = getc(file))!= EOF)
		{
			if (isupper(c))
			{
				numupper++;
			}

			if (islower(c))
			{
				numlower++;
			}

			if ((!isalpha(c) && !isdigit(c) && !isspace(last) && !ispunct(c)) || c ==EOF)
			{
				numword++;
			}

			if (isdigit(c))
			{
				numdigit++;
			}

			if (ispunct(c))
			{
				numpunc++;
			}
			last = c;
		}	
		printf("There are %i words, %i uppercase letters, %i lowercase letters, %i punctuation characters and %i digits", numword, numupper, numlower, numpunc, numdigit);
		fclose(file);
	}
	else
	{
		printf("File not found");
	}

	return 0;
}